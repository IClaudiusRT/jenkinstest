package base;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestMethodsTest {

    @Test
    public void returnStringTest(){
        TestMethods testMethods = new TestMethods();
        assertEquals(testMethods.returnString("tybe"), "Hello tybe");
    }

}